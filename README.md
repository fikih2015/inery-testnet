<p align="center">
  <img width="300" height="auto" src="https://user-images.githubusercontent.com/108969749/201539843-7eede5ae-92ef-456c-baa7-233e8dfba32c.jpeg">
</p>

### Spesifikasi Hardware :
NODE  | CPU     | RAM      | SSD     |
| ------------- | ------------- | ------------- | -------- |
| Testnet | 4          | 8         | 160  |
### Update Package
```
sudo apt update && sudo apt upgrade -y
```
### Install Dependencies
```
sudo apt-get install -y make bzip2 automake libbz2-dev libssl-dev doxygen graphviz libgmp3-dev \
autotools-dev libicu-dev python2.7 python2.7-dev python3 python3-dev \
autoconf libtool curl zlib1g-dev sudo ruby libusb-1.0-0-dev \
libcurl4-gnutls-dev pkg-config patch llvm-7-dev clang-7 vim-common jq libncurses5
```
### Download program node
```
git clone https://github.com/inery-blockchain/inery-node
```
### Memberi Izin File
```
cd inery-node/inery.setup
```
```
chmod +x ine.py
```
```
./ine.py --export
```
```
cd; source .bashrc; cd -
```
### Konfigurasi Master Node
```
cd tools
```
```
nano config.json
```
output

>     "MASTER_ACCOUNT": 
>     "NAME": "AccountName",
>     "PUBLIC_KEY": "PublicKey",
>     "PRIVATE_KEY": "PrivateKey",
>     "PEER_ADDRESS": "IP:9010",
>     "HTTP_ADDRESS": "0.0.0.0:8888",
>     "HOST_ADDRESS": "0.0.0.0:9010"

Ganti sesuai sama yang didashboard Inery, pastikan dengan teliti !
### Run Node
```
cd -
```
```
screen -S inery
./ine.py --master
```
Tutup screen dengan menekan CTRL+ AD
### Cek Log Node
```
tail -f $HOME/inery-node/inery.setup/master.node/blockchain/nodine.log
```
### Membuat Wallet
   ° file.txt berisi password wallet
```
cline wallet create -n <your_name_wallet> -f file.txt
```
   ° Import private key
```
cline wallet import --private-key <your_private_key> -n <your_name_wallet>
```
   ° Unlock wallet
```
cline wallet unlock -n namawallet
```
### Daftar sebagai produser
```
cline system regproducer <your_account> <your_public_key> 0.0.0.0:9010
```
```
cline system makeprod approve <your_account> <your_account>
```
..... semoga membantu .....
